(function(){
    var app = angular.module('wst',['Services']);
    var id = location.search.split('id=')[1];

    app.controller('MentorController', function($scope, $location, ParseService){
        $scope.uploadfile = function(id) {
                    swal({   
                        title:"",
                        text: 'Uploading.......',   
                        imageUrl: 'img/loading.gif',   
                        animation: true,
                        closeOnConfirm: false,   
                        closeOnCancel: false,
                        showConfirmButton: false
                    });
                    var Order = Parse.Object.extend("Order");
                    var query = new Parse.Query(Order);
                    query.get(id,{
                        success:function(result) {
                            

                            var jqueryId= "#" + id;
                            var fileUploadControl = $(jqueryId)[0];
                
                            if (fileUploadControl.files.length > 0) {

                                var file = fileUploadControl.files[0];
                                console.log(file.name);
                                var name = file.name;
                                var parseFile = new Parse.File(name, file);
                            }
                                parseFile.save().then(function() {
                                    result.set("resumeRevice", parseFile);
                                    result.set("revice", true);
                                    result.save(null, {
                                        success:function(result){
                                            swal({ 
                                                title:"",
                                                text: "Upload Succeed!", 
                                                type: "success", 
                                                confirmButtonColor: "#7d0022"
                                            },
                                            function(){
                                                window.location.reload();
                                            });
                                        },
                                        error:function(error){
                                            swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#7d0022"
                                            }); 
                                        }
                                    });
                                });

                        }
                    });
        };

        ParseService.getMentorIfo(id, function(result) {
            $scope.$apply(function() {
                $scope.info = result;
                //console.log(results[0].get('Mentor'));
                //console.log(result.get("phone"));
            });
        });
        
        ParseService.MentorOrders(id, function(results) {
            $scope.$apply(function() {
                $scope.orders = results;
                //console.log(results[0].get('Mentor'));
                //console.log(result.get("phone"));
            });
        });
        ParseService.MentorHistoryOrders(id, function(results) {
            $scope.$apply(function() {
                $scope.histories = results;
                //console.log(results[0].get('Mentor'));
                //console.log(result.get("phone"));
            });
        });
        $scope.confirm = function(id) {
            swal({   
                        title:"",
                        text: 'Processing.......',   
                        imageUrl: 'img/loading.gif',   
                        animation: true,
                        closeOnConfirm: false,   
                        closeOnCancel: false,
                        showConfirmButton: false
                    });
            var copyOf = new Date();
            copyOf.setTime( copyOf.getTime() +  86400000 );
            var Order = Parse.Object.extend("Order");
            var orderQuery = new Parse.Query(Order);
          orderQuery.get(id,{
            success:function(result) {
              result.set("confirm", true);
              result.set('ExpireTime', copyOf);
              result.save({
                success:function(result) {
                    swal({ 
                                                title:"",
                                                text: "Confirm Succeed!", 
                                                type: "success", 
                                                confirmButtonColor: "#7d0022"
                                            },
                                            function(){
                                                window.location.reload();
                                            });
                    window.location.reload();
                },
                error:function(error) {
                    swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#7d0022"
                                            });
                    console.log(error);
                }
              });
            },
            error:function(error){
                swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#7d0022"
                                            });
              console.log(error);
            }
          });
        };
        $scope.cancel = function(id) {
            if($("#reason"+id).val() == '') {
                
                swal({ 
                    title:"",
                    text: "Please input reason", 
                    type: "warning", 
                    confirmButtonColor: "#FFD246"
                });
                return false;
            }
            console.log($("#reason"+id).val())
            var Order = Parse.Object.extend("Order");
          var orderQuery = new Parse.Query(Order);
          orderQuery.get(id,{
            success:function(result) {
                result.set("finish", true);
                result.set("reason", $("#reason"+id).val());
                result.save({
                    success:function(result) {
                        swal({ 
                                                title:"",
                                                text: "Cancel Succeed!", 
                                                type: "success", 
                                                confirmButtonColor: "#7d0022"
                                            },
                                            function(){
                                                window.location.reload();
                                            });
                    },
                    error:function(error) {
                        swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#7d0022"
                                            });
                        console.log(error);
                    }
                });
            },
            error:function(error){
                swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#7d0022"
                                            });
              console.log(error);
            }
          });
        };
    });
})();

