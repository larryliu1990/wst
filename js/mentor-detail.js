﻿
(function () {
    var check = Parse.User.current();
	if(!check){
		window.stop();
		swal({ 
            title:"",
            text: "Please log in to see the detail information", 
            type: "warning", 
            confirmButtonColor: "#7d0022"
        },
        function(){
           window.location.href="login.html";
        });
	}

	var app = angular.module('Mentor',['Services','ui.bootstrap']);
	
	var id = location.search.split('id=')[1];



	app.controller('MentorController', function($scope,$location,ParseService) {
		ParseService.getMentor(id, function(result) {
			$scope.$apply(function() {
				$scope.mentor = result;
			});
		});	
		
		ParseService.getMentorService(id, function(results) {
			$scope.$apply(function() {
				$scope.services = results;
				//service = results;
			});
		});	

		ParseService.getMentorReview(id, function(results) {
			$scope.$apply(function() {
				$scope.reviews = results;
				$scope.reviewsNum = results.length;
				//service = results;
			});
		});	
		  $scope.checkout = function(amount,id,description) {
            if(!user){
                window.location="./login.html?cart";
            }
            var price = (amount*100).toFixed(0);
            var handler = StripeCheckout.configure({
                        key: 'pk_live_z49YOhZgGQEf4bmUtnssKTkJ',
                        description:description,
                        //image: 'https://s3.amazonaws.com/stripe-uploads/acct_16ZvYhDrAc9eF8OCmerchant-icon-1441837336311-smallLogo.png',
                        locale: 'us',
                        zipCode: 'true',
                        billingAddress: 'true',
                        shippingAddress: 'false',
                        token: function(token, addresses) {

                            swal({   
                                title:"",
                                text: 'Proccessing...',   
                                imageUrl: 'img/loading.gif',   
                                animation: true,
                                closeOnConfirm: false,   
                                closeOnCancel: false,
                                showConfirmButton: false
                            });                         
                            Parse.Cloud.run('charge', { 
                                token: token.id, 
                                email: user.get('email'),
                                amount: Number(price), 
                                currency: 'USD',
                                description:description
                            }, {
                              success: function(result) {
                                console.log(result);
                                Parse.Cloud.run('changeOrder', {
                                    order:id, 
                                    payment : result.default_source
                                },{
                                    success:function(result){
                                        console.log(result);
                                        swal({ 
                                            title:"",
                                            text: "Payment Succeed!", 
                                            type: "success", 
                                            confirmButtonColor: "#7d0022"
                                     
                                        },
                                        function(){   
                                            location.href = "account.html";
                                        });
                                    },
                                    error:function(error){
                                        swal({ 
                                            title:"",
                                            text: "Payment Error, Please Contact Us!", 
                                            type: "error", 
                                            confirmButtonColor: "#7d0022"
                                        }); 
                                    }
                                });
                         
                                
                              },
                              error: function(error) {
                                
                              }
                            });
                            
                        },
                    });
                    
                    handler.open({
                      name: 'Wall Street Tequila',
                      amount: price
                    });
            
        }


		$scope.makeOrder = function() {
			
		    if (!user) {
		    	window.location.href="./login.html";
		    	return false;
		    }

		    if($("#ReserveTime").val() == ""){
				alert("Please Input the Time");
				return false;
			} 

			$('#pageloader').fadeIn('slow');
        	$('.loader-item ').fadeIn('slow');

			var copyOf = new Date(serviceTime.valueOf());
			copyOf.setTime( copyOf.getTime() +  86400000 );
		    var Service = Parse.Object.extend("Service");
		    var serviceQuery = new Parse.Query(Service);
		    serviceQuery.get(serviceID, {
		    		success:function(service) {
		    			var Mentor = Parse.Object.extend("Mentor");
		    			var mentorQuery = new Parse.Query(Mentor);
		    			mentorQuery.get($scope.mentor.id, {
		    				success:function(mentor) {
		    					var Order = Parse.Object.extend("Order");
					            var orderQuery = new Order();
					            orderQuery.set("User", user);
					            orderQuery.set("Service", service);
					            orderQuery.set('Mentor', mentor);
					            orderQuery.set('Time', serviceTime);
					            orderQuery.set('ExpireTime', copyOf);
					            orderQuery.set('price', service.get('price'));
					            orderQuery.set('timezone', mentor.get('timezone'));
					            orderQuery.set('img', mentor.get('img'));
					            orderQuery.set('serviceName', service.get('title'));
					            orderQuery.set('mentorEmail', mentor.get('email'));
					            orderQuery.set('menteeEmail', user.get('email'));
					            orderQuery.set('mentorName', mentor.get('name'));
					            orderQuery.set('menteeName', user.get('name'));
					           	orderQuery.set('message', $("#message").val());
					            orderQuery.set('pay', false);
					            orderQuery.set('confirm', false);
					            orderQuery.set('finish', false);
					            orderQuery.save(null,{
					                success:function(order){
                                            swal({ 
                                                title:"",
                                                text: "Succeed! Please wait for mentor's confirm", 
                                                type: "success", 
                                                confirmButtonColor: "#7d0022"
                                            },
                                            function(){
                                                window.location.href="account.html";
                                            });
					                },
					                error:function(error) {
					                  swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#7d0022"
                                            }); 
					                }
					            });
		    				}, 
		    				error:function(error) {
		    					console.log(error);
		    				}
		    			});
			        },
			        error:function(error){
			        	swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#7d0022"
                                            }); 
			        }
		    });
		    			

		};
		
	});
	
})();

$(document).ready(function(){
	serviceID = "";
	serviceTime = "";
	var today = new Date();
	jQuery('.datetimepicker').datetimepicker({
        onGenerate:function( ct ){
    		$('.datetimepicker').show();
  		},
  		inline:true,
  		allowTimes:['12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00','20:00'],

  		minDate:'+1970/01/02',
  		defaultSelect:false,
        format:'Y/m/d H:i',
        lang:'en',
        minTime:'10:00',
        maxTime:'23:00',
        //defaultTime:'05:00',
        format:'Y/m/d H:i',
        onChangeDateTime:function(dp,$input){
        	serviceTime = dp;
  		}
    });

    $('#exampleModal').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget);// Button that triggered the modal
		  var recipient = button.data('name');
		  var id = button.data('id'); 
		  serviceID = id;
		  $("#ReserveTime").val('');
		  $("#message").val('');
		  console.log(serviceID);
		  // Extract info from data-* attributes
		  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		  var modal = $(this)
		  modal.find('.modal-title').text('Reserve For ' + recipient);
		  modal.find('.modal-body #serviceID').val(id);
	});
});

