(function () {

	var app = angular.module('Blogs',['Services']);
    
	
	

	app.controller('BlogController', function($scope,$location,ParseService) {
		ParseService.getBlogs(function(results) {
			$scope.$apply(function() {
				$scope.blogs = results;
			});
		});

		ParseService.getRecommndMentor(function(results) {
			$scope.$apply(function() {
				$scope.recommndMentor = results;
			});
		});		
	});	
	
})();