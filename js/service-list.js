﻿(function () {
	var app = angular.module('Service',['Services']);
	
	app.controller('ServiceController', function($scope,$location,ParseService) {
		ParseService.getServices(function(results) {
			$scope.$apply(function() {
				$scope.services = results;
			});
		});	
	});
	
})();

