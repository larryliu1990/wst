var login = {
	sginupForm: function(){	
		if ( $( "#signupform" ).length !== 0 ) {
		$('#signupform')
        .find('[name="phone"]')
            .intlTelInput({
                utilsScript: 'js/utils.js',
                autoPlaceholder: true,
                preferredCountries: ['us', 'cn', 'gb']
            });
		$('#signupform').bootstrapValidator({
				container: 'tooltip',
				feedbackIcons: {
					valid: 'fa fa-check',
					warning: 'fa fa-user',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
				},
				fields: { 
					name: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    username: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            stringLength: {
                        		min: 6,
                        		max: 15,
                        		message: 'The username must be more than 6 and less than 15 characters long'
                    		},
                    		regexp: {
                        		regexp: /^[a-zA-Z0-9_\.]+$/,
                        		message: 'The username can only consist of alphabetical, number, dot and underscore'
                    		}
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            emailAddress: {
                                message: ''
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.+-]+\.(edu)$/,
                                message: 'For now we only accept EDU email address to register'
                            }
                        }
                    },
                    wechat: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            regexp: {
                                regexp: /^[a-z_\d]+$/,
                                message: 'Please input the right Wechat ID'
                            }
                        }
                    },
					university: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    major: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    phone: {
                        validators: {
                        	notEmpty: {
                                message: ''
                            },
			                callback: {
			                    message: 'The phone number is not valid',
			                    callback: function(value, validator, $field) {
			                        return value === '' || $field.intlTelInput('isValidNumber');
			                    }
			                }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            stringLength: {
                        		min: 8,
                        		max: 12,
                        		message: 'The password must be more than 8 and less than 12 characters long'
                    		}
                        }
                    },
                    passwordconfirm: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            identical: {
                    			field: 'password',
                    			message: 'The password and its confirm are not the same'
                			}
                        }
                    },
				}
			})
			.on('click', '.country-list', function() {
        		$('#signupform').formValidation('revalidateField', 'phone');
    		})		
			.on('success.form.bv', function(e) {
				e.preventDefault();
				var $form        = $(e.target),
				validator    = $form.data('bootstrapValidator'),
				submitButton = validator.getSubmitButton();
				var form_data = $('#signupform').serializeArray();
				var country = $("#signupform input[name*='phone']").intlTelInput("getSelectedCountryData").iso2;
				Parse.User.logOut();
				Parse.Cloud.run('SignUp', { 
					name: form_data[0].value,
					username: form_data[1].value, 					
					email: form_data[2].value, 
                    wechat: form_data[2].value, 
					university : form_data[4].value,
					major : form_data[5].value,
					country: country,
					phone: form_data[6].value,
					password: form_data[7].value
				}, {
	              success: function(result) {
	              	submitButton.removeAttr("disabled");
					//window.location.href="/emailVerify";
                    //window.history.back();
                    user=result;
                    window.location.href="/account.html"
	              },
	              error: function(error) {
	              	alert("Error: " + error.message);
	              }
	            });
			});
		}
	},
	loginForm: function(){	
		if ( $( "#loginform" ).length !== 0 ) {
		$('#loginform').bootstrapValidator({
				container: 'tooltip',
				feedbackIcons: {
					valid: 'fa fa-check',
					warning: 'fa fa-user',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
				},
				fields: { 
					username: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            stringLength: {
                        		min: 6,
                        		max: 15,
                        		message: 'The username must be more than 6 and less than 15 characters long'
                    		},
                    		regexp: {
                        		regexp: /^[a-zA-Z0-9_\.]+$/,
                        		message: 'The username can only consist of alphabetical, number, dot and underscore'
                    		}
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            stringLength: {
                        		min: 8,
                        		max: 12,
                        		message: 'The password must be more than 8 and less than 12 characters long'
                    		}
                        }
                    },
				}
			})	
			.on('success.form.bv', function(e) {
				e.preventDefault();
				$("#loader").fadeIn();
				$(".loader-container").fadeIn();
				var $form        = $(e.target),
				validator    = $form.data('bootstrapValidator'),
				submitButton = validator.getSubmitButton();
				var form_data = $('#loginform').serializeArray();
				Parse.User.logOut();
				Parse.User.logIn(form_data[0].value, form_data[1].value, {
      	    		success: function(user) {
              			//loggedInUser = user;
      	      			//console.log(user);
      	      			submitButton.removeAttr("disabled");
      	      			/*var url = location.search.substring(1);
    					if (url == "Services") {
    						window.location.href="service";
    					} else {
    						window.location.href="profile";
    					}*/
                        window.history.back();
                        //window.location.href="/account.html";
      	    		},
      	    		error: function(error) {
      	    			$(".loader-item").fadeOut();
						$("#pageloader").fadeOut("slow");
	      	      		swal({ 
	                        title:"",
	                        text: error.code + error.message, 
	                        type: "error", 
	                        confirmButtonColor: "#7d0022"
	                    });
      	    		}
          		});
				
			});
		}
	},
};
$(document).ready(function(){
	login.loginForm();
	login.sginupForm();
});

function forgotPass() {
    swal({   
        title: "Password Reset",   
        text: "Please input your email:",   
        type: "input",   
        showCancelButton: true,   
        closeOnConfirm: false,   
        animation: "slide-from-top",  
        inputPlaceholder: "Email address",
        confirmButtonColor: "#7d0022"
    }, function(inputValue){   
        if (inputValue === false) return false;      
        if (inputValue === "") {     
                swal.showInputError("You need to write the email!");     
                return false   
            } 
            Parse.User.requestPasswordReset(inputValue, {
                success: function() {
                    swal({
                        title: "Nice!", 
                        text: "Password reset link has been sent to " + inputValue, 
                        type: "success",
                        confirmButtonColor: "#7d0022"
                    }); 
                },
                error: function(error) {
                    swal({ 
                        title:"",
                        text: error.code + error.message, 
                        type: "error", 
                        confirmButtonColor: "#7d0022"
                    });
                }
            });     
            
        });
}