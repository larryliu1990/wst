 var img1="";

(function(){
    img1="";
    var app = angular.module('wst',['Services']);

    app.controller('UserInfoController', function($scope, $location, ParseService){
        $scope.uploadfile = function(id) {
                    swal({   
                        title:"",
                        text: 'Uploading........',   
                        imageUrl: 'img/loading.gif',   
                        animation: true,
                        closeOnConfirm: false,   
                        closeOnCancel: false,
                        showConfirmButton: false
                    });
                    var Order = Parse.Object.extend("Order");
                    var query = new Parse.Query(Order);
                    query.get(id,{
                        success:function(result) {
                            var jqueryId1 = "#demand" + id;
                            var jqueryId= "#" + id;
                            var fileUploadControl = $(jqueryId)[0];
                
                            if (fileUploadControl.files.length > 0) {

                                var file = fileUploadControl.files[0];
                                console.log(file.name);
                                var name = file.name;
                                var parseFile = new Parse.File(name, file);
                            }
                                parseFile.save().then(function() {
                                    result.set("original", parseFile);
                                    result.set("upload", true);
                                    result.save(null, {
                                        success:function(result){
                                            swal({ 
                                                title:"",
                                                text: "Upload Succeed", 
                                                type: "success", 
                                                confirmButtonColor: "#7d0022"
                                            },
                                            function(){
                                                window.location.reload();
                                            });
                                        },
                                        error:function(error){
                                            swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#7d0022"
                                            }); 
                                        }
                                    });
                                });

                        }
                    });
        };
        $scope.checkout = function(amount,id,description) {
            if(!user){
                window.location="./login.html?cart";
            }
            var price = (amount*100).toFixed(0);
            var handler = StripeCheckout.configure({
                        key: 'pk_live_z49YOhZgGQEf4bmUtnssKTkJ',
                        description:description,
                        //image: 'https://s3.amazonaws.com/stripe-uploads/acct_16ZvYhDrAc9eF8OCmerchant-icon-1441837336311-smallLogo.png',
                        locale: 'us',
                        zipCode: 'true',
                        billingAddress: 'true',
                        shippingAddress: 'false',
                        token: function(token, addresses) {

                            swal({   
                                title:"",
                                text: 'Proccessing...',   
                                imageUrl: 'img/loading.gif',   
                                animation: true,
                                closeOnConfirm: false,   
                                closeOnCancel: false,
                                showConfirmButton: false
                            });                         
                            Parse.Cloud.run('charge', { 
                                token: token.id, 
                                email: user.get('email'),
                                amount: Number(price), 
                                currency: 'USD',
                                description:description
                            }, {
                              success: function(result) {
                                console.log(result);
                                Parse.Cloud.run('changeOrder', {
                                    order:id, 
                                    payment : result.default_source
                                },{
                                    success:function(result){
                                        console.log(result);
                                        swal({ 
                                            title:"",
                                            text: "Payment Succeed!", 
                                            type: "success", 
                                            confirmButtonColor: "#7d0022"
                                     
                                        },
                                        function(){   
                                            location.href = "account.html";
                                        });
                                    },
                                    error:function(error){
                                        swal({ 
                                            title:"",
                                            text: "Payment Error, Please Contact Us!", 
                                            type: "error", 
                                            confirmButtonColor: "#7d0022"
                                        }); 
                                    }
                                });
                         
                                
                              },
                              error: function(error) {
                                
                              }
                            });
                            
                        },
                    });
                    
                    handler.open({
                      name: 'Wall Street Tequila',
                      amount: price
                    });
            
        }

        var id = Parse.User.current().id;
        ParseService.getUserInfo(id, function(result) {
            $scope.$apply(function() {
                $scope.product = result;
                console.log(result.get('name'));
                angular.element('#profileForm input[name*="phone"]').intlTelInput("selectCountry", result.get('country'));
                angular.element('#profileForm input[name*="phone"]').intlTelInput("setNumber", result.get('phone'));
                //console.log(result.get("phone"));
            });
        });
        ParseService.getUserOrders(id, function(results) {
            $scope.$apply(function() {
                $scope.orders = results;
                //console.log(results[0].get('Mentor'));
                //console.log(result.get("phone"));
            });
        });
        ParseService.getHistoryOrders(id, function(results) {
            $scope.$apply(function() {
                $scope.histories = results;
                //console.log(results[0].get('Mentor'));
                //console.log(result.get("phone"));
            });
        });
    });
})();

var appMaster = {
    checkLogin: function(){
        user = Parse.User.current();
        if(!user){
            window.location="login.html";
        }
    },
    updateForm: function(){ 
        if ( $( "#profileForm" ).length !== 0 ) {
            $('#profileForm')
            .find('[name="phone"]')
                .intlTelInput({
                    utilsScript: 'js/utils.js',
                    autoPlaceholder: true,
                    preferredCountries: ['us', 'cn', 'gb']
                });
                $('#profileForm')
            .bootstrapValidator({
                    container: 'tooltip',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        warning: 'fa fa-user',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: { 
                        name: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                },
                                emailAddress: {
                                    message: ''
                                }
                            }
                        },
                        university: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                }
                            }
                        },
                        major: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                }
                            }
                        },
                        phone: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                },
                                callback: {
                                    message: 'The phone number is not valid',
                                    callback: function(value, validator, $field) {
                                        return value === '' || $field.intlTelInput('isValidNumber');
                                    }
                                }
                            }
                        },
                        password: {
                            validators: {
                                stringLength: {
                                    min: 8,
                                    max: 12,
                                    message: 'The password must be more than 8 and less than 12 characters long'
                                }
                            }
                        },
                        passwordconfirm: {
                            validators: {
                                identical: {
                                    field: 'password',
                                    message: 'The password and its confirm are not the same'
                                }
                            }
                        },
                    }
                })
                .on('click', '.country-list', function() {
                    $('#profileForm').formValidation('revalidateField', 'phone');
                })  
                .on('success.form.bv', function(e) {
                     swal({   
                        title:"",
                        text: 'Updating',   
                        imageUrl: 'img/loading.gif',   
                        animation: true,
                        closeOnConfirm: false,   
                        closeOnCancel: false,
                        showConfirmButton: false
                    });
                    e.preventDefault();
                    var $form        = $(e.target),
                    validator    = $form.data('bootstrapValidator'),
                    submitButton = validator.getSubmitButton();
                    var form_data = $('#profileForm').serializeArray();
                    var country = $("#profileForm input[name*='phone']").intlTelInput("getSelectedCountryData").iso2;
                    console.log(form_data);

                    Parse.Cloud.run('Update', {
                        user: Parse.User.current().id, 
                        name: form_data[0].value,
                        university: form_data[1].value, 
                        major: form_data[2].value, 
                        wechat: form_data[3].value,
                        phone: form_data[4].value, 
                        email: form_data[5].value,  
                        password: form_data[6].value, 
                        country: country,
                        file:img1
                    }, {
                      success: function(result) {
                        
                        Parse.User.current().fetch();
                        if(result == "pass") {
                            Parse.User.logOut();
                            window.location="./login.html";
                        }
                        swal({ 
                            title:"",
                            text: "Update Succeed!", 
                            type: "success", 
                            confirmButtonColor: "#FFD246",
                            closeOnConfirm: false,  
                        });
                        //window.location="profile.html?login";
                      },
                      error: function(error) {
                        submitButton.removeAttr("disabled");
                      }
                    });
                });
            }
    },
};

$(document).ready(function(){
    appMaster.checkLogin();
    appMaster.updateForm();
    $('.dropzone').html5imageupload({
        buttonZoomreset: true,
                originalsize: false,
                resize: true,
        onSave: function(data) {
            var con = document.getElementById('canvas_thumb');
            var img = con.toDataURL("img/png");
            var start = Number(img.indexOf(",")) + 1;
            var end = Number(img.length);
            var test = img.substring(start, end)
            //console.log(img);
            //console.log(test);
            var parseFile = new Parse.File("photo.jpg", {base64:test});
            parseFile.save().then(function(){
                img1 = parseFile;
            });
            
        }
    });
    var allowTimes = ["10:00","11:00","13:00"];
    var disabledDates = ['11.01.2015','12.01.2015','01.01.2016','02.01.2016','03.01.2016'];
        jQuery('.datetimepicker').datetimepicker({
            onGenerate:function( ct ){
    jQuery(this).find('.xdsoft_date')
      .toggleClass('xdsoft_disabled');
  },
  inline:true,
            format:'Y/m/d H:i',
            lang:'en',
            allowTimes:allowTimes,
            disabledDates: disabledDates, formatDate:'m.d.Y',
             onChangeDateTime:function(dp,$input){
    alert($input.val())
  }
        });
});

function uploadfile() {
                    swal({   
                        title:"",
                        text: 'Uploading',   
                        imageUrl: 'img/loading.gif',   
                        animation: true,
                        closeOnConfirm: false,   
                        closeOnCancel: false,
                        showConfirmButton: false
                    });
                    var query = new Parse.Query(Parse.User);
                    query.get(Parse.User.current().id,{
                        success:function(result) {
                            var fileUploadControl = $("#uploadButton")[0];
                
                            if (fileUploadControl.files.length > 0) {

                                var file = fileUploadControl.files[0];
                                console.log(file.name);
                                var name = file.name;
                                var parseFile = new Parse.File(name, file);
                            }
                                parseFile.save().then(function() {
                                    result.set("resume", parseFile);
                                    result.set("upload", true);
                                    result.save(null, {
                                        success:function(result){
                                            swal({ 
                                                title:"",
                                                text: "Upload Succeed!", 
                                                type: "success", 
                                                confirmButtonColor: "#FFD246" 
                                            },
                                            function(){
                                                window.location.reload();
                                            });
                                        },
                                        error:function(error){
                                            swal({ 
                                                title:"",
                                                text: error.code + error.message, 
                                                type: "error", 
                                                confirmButtonColor: "#FFD246" 
                                            }); 
                                        }
                                    });
                                });

                        }
                    });
        };