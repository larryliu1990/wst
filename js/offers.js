(function () {
	var app = angular.module('Offer',['Services']);
	
	app.controller('OfferController', function($timeout,$scope,$location,ParseService) {
		ParseService.getOffers( 2016,function(results) {
			$scope.$apply(function() {
				$scope.offers2016 = results;
				//console.log(results);
				$scope.$watch("offers2016", function (newValue, oldValue) {
  					$timeout(function() {
  						$('.2016').magnificPopup({
  							delegate: '.image-link',
                    		type:'image',
                    		closeOnContentClick: true
                		});
  					});
  				});
			});
		});

	});
	
})();