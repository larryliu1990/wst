var appMaster = {
    contactForm: function(){ 
        if ( $( "#contactform" ).length !== 0 ) {
                $('#contactform').bootstrapValidator({
                    container: 'tooltip',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        warning: 'fa fa-user',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: { 
                        name: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                },
                                emailAddress: {
                                    message: ''
                                }
                            }
                        },
                        school: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                }
                            }
                        },
                        subject: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                }
                            }
                        },
                        comments: {
                            validators: {
                                notEmpty: {
                                    message: ''
                                }
                            }
                        },
                    }
                })
                .on('success.form.bv', function(e) {
                     $('#pageloader').fadeIn();
                    $('.loader-item ').fadeIn();
                    e.preventDefault();
                    var $form        = $(e.target),
                    validator    = $form.data('bootstrapValidator'),
                    submitButton = validator.getSubmitButton();
                    var form_data = $('#contactform').serializeArray();
                    console.log(form_data);

                    Parse.Cloud.run('sendEmail', {
                        name: form_data[0].value,
                        email: form_data[1].value, 
                        school: form_data[2].value, 
                        subjet: form_data[3].value,
                        message: form_data[4].value
                    }, {
                      success: function(result) {
                        $("#message").text(result)
                        $("#message").show();
                        $('#pageloader').fadeOut();
                        $('.loader-item ').fadeOut();
                
                      },
                      error: function(error) {
                        submitButton.removeAttr("disabled");
                        $("#message").text(error)
                        $("#message").show();
                        $('#pageloader').fadeOut();
                        $('.loader-item ').fadeOut();
                      }
                    });
                });
            }
    },
};

$(document).ready(function() {
    appMaster.contactForm();
});