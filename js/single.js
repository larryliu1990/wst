(function () {

	var app = angular.module('Single',['Services','ngSanitize']);
    
	var id = location.search.split('id=')[1];
	

	app.controller('SingleController', function($scope,$location,$sce,ParseService) {
		ParseService.getBlog(id, function(result) {
			$scope.$apply(function() {
				$scope.blog = result;
				$scope.thisCanBeusedInsideNgBindHtml = $sce.trustAsHtml(result.get('description'));
			});
		});	
		ParseService.getRecommndMentor(function(results) {
			$scope.$apply(function() {
				$scope.recommndMentor = results;
			});
		});		
	});
	

	
	
	
})();