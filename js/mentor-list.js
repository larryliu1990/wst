﻿(function () {
	$('.selectpicker').selectpicker();
	var app = angular.module('Mentor',['Services','ui.bootstrap']);
    filter = location.search.split('search=')[1];
	
	mentorLocation = getOneCondition("location", filter);
	industry = getOneCondition("industry", filter);
	
	//假设前台点击了location, 这样reload:
	//reload(filter, "location=", "United%20States");
	
	currentPage = Number(getOneCondition("page", filter));
	if (currentPage <= 0){
		currentPage = 1;
	}
	
	perPage = 12;
	globalCount = 0;

	app.controller('MentorController', function($scope,$location,ParseService) {
		ParseService.getMentors(perPage, filter, function(results) {
			$scope.$apply(function() {
				$scope.mentors = results;
			});
		});

		ParseService.getMentorCount(filter, function(count) {
			$scope.$apply(function() {
				$scope.count = count;
				globalCount = count;
				console.log(count);
				showPage(count);
			});
		});		
	});
	
	
	$("#page-next").click(function(){
        if (currentPage >= Math.ceil(globalCount / perPage)){
            $("#already-last").show().delay(1000).fadeOut();
            return;
        } 
        currentPage = currentPage + 1;
        reloadPage(filter, "page=", currentPage);
    });
	
	$("#page-prev").click(function(){
        if (currentPage <= 1){
            $("#already-first").show().delay(1000).fadeOut();
            return;
        }
        currentPage = currentPage - 1;
        reloadPage(filter, "page=", currentPage);
    });
	
	$("#page-1").click(function(){
        reloadPage(filter, "page=", Number($(this).text()));
    });
    
    $("#page-2").click(function(){
        reloadPage(filter, "page=", Number($(this).text()));
    });
    
    $("#page-3").click(function(){
        reloadPage(filter, "page=", Number($(this).text()));
    });
    
    $("#page-4").click(function(){
        reloadPage(filter, "page=", Number($(this).text()));
    });
    
    $("#page-5").click(function(){
       reloadPage(filter, "page=", Number($(this).text()));
    });
	
})();

$( document ).ready(function() {
	if(mentorLocation == "") {
		$('input:radio[name="location"]').filter('[value="1"]').attr('checked', true);
	} else {
		checked(mentorLocation);
	}

	if(industry == "") {
		$('input:radio[name="industry"]').filter('[value="1"]').attr('checked', true);
	} else {
		checked(industry);
	}


	$('#location input').on('change', function() {
		var location = $('input[name=location]:checked','#location').val();
		//alert(location);
		reloadPage(filter, "location=", location);
	});
	
	$("#industry").change(function(){
		var industry = $('input[name=industry]:checked','#industry').val();
		//alert(industry);
		reloadPage(filter, "industry=", industry);
	});
	
});

function checked(str) {
		document.getElementById(str).checked = true;
}

function showPage(count){

    pageNumber = Math.ceil(count / perPage);
    console.log(pageNumber);
	var pageNumber = Math.ceil(count / perPage);
	var rest = pageNumber - currentPage;
				
	if (pageNumber > 1){
		$("#pagination").show();
	}
	
	if (pageNumber < 6){
		$("#page-more").hide();
	}
	if (pageNumber < 5){
		$("#page-5").hide();
	}
	if (pageNumber < 4){
		$("#page-4").hide();
	}
	if (pageNumber < 3){
		$("#page-3").hide();
	}
	if (pageNumber < 2){
		$("#page-2").hide();
	}
	
	if (currentPage < 5) {
		var pageId = "#page-" + (currentPage) + "-li";
		$(pageId).addClass("active");
	} else {
		$("#page-5-li").addClass("active");
		$("#page-1").text(Number(currentPage - 4));
		$("#page-2").text(Number(currentPage - 3));
		$("#page-3").text(Number(currentPage - 2));
		$("#page-4").text(Number(currentPage - 1));
		$("#page-5").text(Number(currentPage));
	}
}

/*
function reloadPage(filter, currentPage){
	if(typeof filter == "undefined"){
		window.location.replace("mentor-list.html?search=page=" + currentPage + "&");
		return;
	} else {
		var url = "mentor-list.html?search=" + filter;
		var start = url.indexOf("page=");
		if (start == -1){
			url = url + "page=" + currentPage + "&";
		} else {
			var end = url.substring(start).indexOf("&");
			url = url.substring(0, start) + "page=" + currentPage + "&" + url.substring(url.substring(0, start).length + 1 + end);
		}
		window.location.replace(url);
	}
}
*/

function reloadPage(filter, conditionName, condition){
	if (condition == 1) {
		window.location.replace("mentor-list.html");
	} else {
		if(typeof filter == "undefined"){
			window.location.replace("mentor-list.html?search=" + conditionName + condition + "&");
			return;
		} else {
			var url = "mentor-list.html?search=" + filter;
			var start = url.indexOf(conditionName);
			if (start == -1){
				url = url + conditionName  + condition + "&";
			} else {
				var end = url.substring(start).indexOf("&");
				url = url.substring(0, start) + conditionName +  condition + "&" + url.substring(url.substring(0, start).length + 1 + end);
			}
			window.location.replace(url);
		}
	}
}

function getOneCondition(condition, conditions){
	if(typeof conditions == "undefined"){
		return "";
	}
	var start = conditions.indexOf(condition + "=");
	if(start == -1){
		return "";
	}
	var sub = conditions.substring(start);
	var end = sub.indexOf("&");
	return sub.substring(condition.length + 1, end);
}
