
(function () {
  'use strict';

  /* Services */

  angular.module('Services', ['ngResource'])
  .factory('ParseService', function($resource){
      // Initialize Parse API and objects.
      Parse.initialize("x2CXqVITh3IqN1yQ4v8906tNIbKKeufNgYO7vIwb", "GKtJBLf9fXyB1KKSZPgE5CCRGT6IOK4rYAFoDWt0");

      // Cache current logged in user
      

     
      var ParseService = {
        name: "Parse",

        // Login a user
        login : function login(username, password, callback) {
      	  Parse.User.logIn(username, password, {
      	    success: function(user) {
              loggedInUser = user;
      	      callback(user);
      	    },
      	    error: function(user, error) {
      	      alert("Error: " + error.message);
      	    }
          });
        },

        // Logout current user
        logout : function logout(callback) {
          Parse.User.logOut();
        },

        /*getMentors : function getMentors(str,callback) {

          var query = new Parse.Query(str);

          query.find({
            success : function(results) {
              callback(results);
              $(".rating").raty({ score: 3 });
            },
            error: function(error) {
              alert("Error: " + error.message);
            }
          });
        },*/

        // Get all books belonging to logged in user
        getMentorsDe : function getMentorsDe(str,id,callback) {
          // Create a new Parse Query to search Book records by ownerid
          var query = new Parse.Query(str);
          query.get(id,{
            success : function(result) {
              
              callback(result);
            },
            error: function(error) {
              alert("Error: " + error.message);
            }
          });
        },


        getCourse : function getCourse(id,num,callback) {
          // Create a new Parse Query to search Book records by ownerid
          var query = new Parse.Query(Course);
          query.get(id,{
            success : function(result) {
              var video = result;
              callback(video);
            },
            error: function(error) {
              alert("Error: " + error.message);
            }
          });
        },

        getUserInfo : function getUserInfo(id,callback) {
          Parse.Cloud.run('UserInfo', {
            user:id
          },{
            success:function(result){
              callback(result);
            },
            error:function(error){
              alert("Error: " + error.message);
            }
          });
        },
        getProfile : function getProfile(id,callback) {
          Parse.Cloud.run('UserProfile', {
            user:id
          },{
            success:function(result){
              callback(result);
            },
            error:function(error){
              alert("Error: " + error.message);
            }
          });
        },
		
		getServices : function getServices(callback) {
        var Service = Parse.Object.extend("Service");
			  var serviceQuery = new Parse.Query(Service);
        serviceQuery.ascending('order');
			serviceQuery.find({
			  success: function(results) {
				callback(results);
			  },
			  error: function(error) {
				console.log(error);
				// The request failed
			  }
			});
        },

    getOffers : function getOffers(num, callback) {
        var offers = Parse.Object.extend("Offers");
        var offersQuery = new Parse.Query(offers);
        //offersQuery.equalTo("year",num);
        offersQuery.find({
          success: function(results) {
            callback(results);
            console.log(results);
          },
          error: function(error) {
            console.log(error);
            // The request failed
          }
        });
    },
		
		getMentors : function getMentors(perPage, filter, callback) {
			var mentorQuery = filterQuery(filter);
			
			var currentPage = Number(getOneCondition("page", filter));
			if (currentPage <= 0){
				currentPage = 1;
			}
			
			mentorQuery.limit(perPage);
			
			mentorQuery.skip((currentPage - 1) * perPage);

			mentorQuery.find({
			  success: function(results) {
				callback(results);
			  },
			  error: function(error) {
				console.log(error);
				// The request failed
			  }
			});
        },
		
		getMentor : function getMentor(id, callback) {
        	var Mentor = Parse.Object.extend("Mentor");
			     var mentorQuery = new Parse.Query(Mentor);

			mentorQuery.get(id, {
			  success: function(result) {
				callback(result);
			  },
			  error: function(error) {
				console.log(error);
				// The request failed
			  }
			});
    },

    getMentorReview : function getMentorReview (id, callback) {
      var Mentor = Parse.Object.extend("Mentor");
      var mentorQuery = new Parse.Query(Mentor);

      mentorQuery.get(id, {
        success: function(mentor) {
          var Review = Parse.Object.extend("Review");
          var reviewQuery = new Parse.Query(Review);
          reviewQuery.equalTo("Mentor", mentor);
          reviewQuery.find({
            success:function(results) {
              callback(results);
            },
            error:function(error) {
              console.log(error);
            }
          });
        },
        error: function(error) {
        console.log(error);
        // The request failed
        }
      });
    },
		
		getMentorCount : function getMentorCount(filter, callback) {
			var mentorQuery = filterQuery(filter);
			mentorQuery.count({
			  success: function(count) {
				callback(count);
			  },
			  error: function(error) {
				console.log(error);
				// The request failed
			  }
			});
    },
		
		getMentorService : function getMentorService(id, callback) {
        	var Mentor = Parse.Object.extend("Mentor");
			var mentorQuery = new Parse.Query(Mentor);

			mentorQuery.get(id, {
			  success: function(result) {
					var services = result.get("Service");
					var Service = Parse.Object.extend("Service");
					var serviceQuery = new Parse.Query(Service);
					serviceQuery.containedIn("objectId", services);
					serviceQuery.find({
					  success: function(results) {
						callback(results);
					  },
					  error: function(error) {
						console.log(error);
						// The request failed
					  }
					});
					
					
			  },
			  error: function(error) {
				console.log(error);
				// The request failed
			  }
			});
    },

    getUserOrders : function getUserOrders(id, callback) {
      var Order = Parse.Object.extend("Order");
      var orderQuery = new Parse.Query(Order);
      var user = Parse.User.current();
      orderQuery.equalTo("User", user);
      orderQuery.equalTo("finish", false);
      orderQuery.descending("updatedAt");
      orderQuery.find({
        success:function(results) {
          callback(results);
        },
        error:function(error) {
          console.log(error);
        }
      });
    },

		getHistoryOrders : function getHistoryOrders (id, callback) {
      var Order = Parse.Object.extend("Order");
      var orderQuery = new Parse.Query(Order);
      var user = Parse.User.current();
      orderQuery.equalTo("User", user);
      orderQuery.equalTo("finish", true);
      orderQuery.descending("updatedAt");
      orderQuery.find({
        success:function(results) {
          callback(results);
        },
        error:function(error) {
          console.log(error);
        }
      });
    },

    MentorOrders : function MentorOrders(id, callback) {
      var Mentor = Parse.Object.extend("Mentor");
      var mentorQuery = new Parse.Query(Mentor);
      mentorQuery.get(id, {
        success:function(mentor) {
          var Order = Parse.Object.extend("Order");
          var orderQuery = new Parse.Query(Order);
          orderQuery.equalTo("Mentor", mentor);
          orderQuery.equalTo("finish", false);
          orderQuery.descending("updatedAt");
          orderQuery.find({
            success:function(results) {
              callback(results);
            },
            error:function(error){
              console.log(error);
            }
          });
        },
        error:function(error) {
          console.log(error);
        }
      });
    },

    MentorHistoryOrders : function MentorHistoryOrders(id, callback) {
      var Mentor = Parse.Object.extend("Mentor");
      var mentorQuery = new Parse.Query(Mentor);
      mentorQuery.get(id, {
        success:function(mentor) {
          var Order = Parse.Object.extend("Order");
          var orderQuery = new Parse.Query(Order);
          orderQuery.equalTo("Mentor", mentor);
          orderQuery.equalTo("finish", true);
          orderQuery.descending("updatedAt");
          orderQuery.find({
            success:function(results) {
              callback(results);
            },
            error:function(error){
              console.log(error);
            }
          });
        },
        error:function(error) {
          console.log(error);
        }
      });
    },

    getMentorIfo : function getMentorIfo(id,callback) {
      var Mentor = Parse.Object.extend("Mentor");
      var mentorQuery = new Parse.Query(Mentor);
      mentorQuery.get(id, {
        success:function(result) {
          callback(result);
        },
        error:function(error) {
          console.log(error);
        }
      });
    },

    getBlogs : function getBlogs(callback) {
      var Blog = Parse.Object.extend("Blog");
      var blogQuery = new Parse.Query(Blog);
      blogQuery.descending('updatedAt');
      blogQuery.find({
        success:function(results){
          callback(results);
        },
        error:function(error) {
          console.log(error);
        }
      });
    },

    getBlog : function getBlog(id,callback) {
      var Blog = Parse.Object.extend("Blog");
      var blogQuery = new Parse.Query(Blog);
      blogQuery.get(id,{
        success:function(results){
          callback(results);
        },
        error:function(error) {
          console.log(error);
        }
      });
    }, 
    getRecommndMentor : function getRecommndMentor(callback) {
          var Mentor = Parse.Object.extend("Mentor");
      var mentorQuery = new Parse.Query(Mentor);
      mentorQuery.limit(9);
      mentorQuery.find({
        success: function(results) {
        callback(results);
        },
        error: function(error) {
        console.log(error);
        // The request failed
        }
      });
    }, 
  };

      // The factory function returns ParseService, which is injected into controllers.
      return ParseService;
  });
})();

function filterQuery(filter){
	var Mentor = Parse.Object.extend("Mentor");
	var mentorQuery = new Parse.Query(Mentor);
	
	var location = getOneCondition("location", filter);
	if(location){
		location = location.replace(/%20/g, " ");
		mentorQuery.equalTo("locationDe", location);
	}
	
	var industry = getOneCondition("industry", filter);
	if(industry){
		industry = industry.replace(/%20/g, " ");
		mentorQuery.equalTo("industryFocusDe", industry);
	}
	
	return mentorQuery;
}

function getOneCondition(condition, conditions){
  if(typeof conditions == "undefined"){
    return "";
  }
  var start = conditions.indexOf(condition + "=");
  if(start == -1){
    return "";
  }
  var sub = conditions.substring(start);
  var end = sub.indexOf("&");
  return sub.substring(condition.length + 1, end);
}
