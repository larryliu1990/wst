Parse.initialize("x2CXqVITh3IqN1yQ4v8906tNIbKKeufNgYO7vIwb", "GKtJBLf9fXyB1KKSZPgE5CCRGT6IOK4rYAFoDWt0");

var init = {
    checkLogin: function(){
        user = Parse.User.current();
        if(user){
            console.log(user);
            document.getElementById('indexUsername').insertAdjacentHTML("beforeEnd",user.get('name'));
            $("#login").hide();
            $("#regnow").hide();
            $("#user").show();
        }
    },
    loginForm: function(){  
        if ( $( "#indexLogin" ).length !== 0 ) {
        $('#indexLogin').bootstrapValidator({
                container: 'tooltip',
                feedbackIcons: {
                    valid: 'fa fa-check',
                    warning: 'fa fa-user',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                fields: { 
                    username: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            stringLength: {
                                min: 6,
                                max: 15,
                                message: 'The username must be more than 6 and less than 15 characters long'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: 'The username can only consist of alphabetical, number, dot and underscore'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            stringLength: {
                                min: 8,
                                max: 12,
                                message: 'The password must be more than 8 and less than 12 characters long'
                            }
                        }
                    },
                }
            })  
            .on('success.form.bv', function(e) {
                e.preventDefault();
                $("#loader").fadeIn();
                $(".loader-container").fadeIn();
                var $form        = $(e.target),
                validator    = $form.data('bootstrapValidator'),
                submitButton = validator.getSubmitButton();
                var form_data = $('#indexLogin').serializeArray();
                Parse.User.logOut();
                Parse.User.logIn(form_data[0].value, form_data[1].value, {
                    success: function(user) {
                        //loggedInUser = user;
                        //console.log(user);
                        window.location.reload();                        
                    },
                    error: function(error) {
                        $(".loader-item").fadeOut();
                        $("#pageloader").fadeOut("slow");
                        swal({ 
                            title:"",
                            text: "There was an error with your username/Password combination. Please try again.", 
                            type: "error", 
                            confirmButtonColor: "#7d0022"
                        });
                    }
                });
                
            });
        }
    },
};

(function($) {
"use strict";

/* ==============================================
LOADER -->
=============================================== */

    $(window).load(function() {
        init.checkLogin();
        init.loginForm();
        $('#pageloader').delay(300).fadeOut('slow');
        $('.loader-item ').delay(200).fadeOut('slow');
        $('body').delay(300).css({'overflow':'visible'});
    })

/* ==============================================
ANIMATION -->
=============================================== */

    new WOW({
	    boxClass:     'wow',      // default
	    animateClass: 'animated', // default
	    offset:       0,          // default
	    mobile:       true,       // default
	    live:         true        // default
    }).init();

/* ==============================================
MENU HOVER -->
=============================================== */

    jQuery('.hovermenu .dropdown').hover(function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    }, function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    });
    jQuery('.topbar .dropdown').hover(function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    }, function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    });

/* ==============================================
CAROUSEL -->
=============================================== */

    $('#owl-featured').owlCarousel({
        loop:true,
        margin:15,
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:4
            }
        }
    })
    $('#owl-featured-2').owlCarousel({
        loop:true,
        margin:15,
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:4
            }
        }
    })

/* ==============================================
LOGIN AJAX -->
=============================================== */

    $('#login-form-link').click(function(e) {
        $("#loginform").delay(100).fadeIn(100);
        $("#signupform").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $("#signupform").delay(100).fadeIn(100);
        $("#loginform").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

/* ==============================================
ACCORDION -->
=============================================== */

    function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('fa-minus fa-plus');
    }
    $('.accordion').bind('hidden.bs.collapse', toggleChevron);
    $('.accordion').bind('shown.bs.collapse', toggleChevron);


/* ==============================================
FUN -->
=============================================== */

    function count($this){
        var current = parseInt($this.html(), 10);
        current = current + 10;
        $this.html(++current);
        if(current > $this.data('count')){
        $this.html($this.data('count'));
        } 
        else {    
        setTimeout(function(){count($this)}, 10);
        }
        }        
        $(".stat-count").each(function() {
        $(this).data('count', parseInt($(this).html(), 10));
        $(this).html('0');
        count($(this));
    });

/* ==============================================
AFFIX -->
=============================================== */

    $('.header').affix({
      offset: {
        top: 100,
        bottom: function () {
          return (this.bottom = $('.footer').outerHeight(true))
        }
      }
    })

    
})(jQuery);

function logout(){
        Parse.User.logOut();
        window.location="/"; 
    }