var Mandrill = require('mandrill');
Mandrill.initialize('dIvST5FcdH0rpOfL0XVEdA');

var Stripe = require('stripe');
Stripe.initialize("sk_live_dD3EVDA75UZLcz2QIaiLQL7u");

var moment = require('cloud/moment-tz');

Parse.Cloud.define("charge", function(request, response) {
  Stripe.Customers.create({
    card: request.params.token,
    //email: request.params.email
    description: request.params.description
  },{
    success: function(results) {
      response.success(results);
    },
    error: function(httpResponse) {
      response.error(httpResponse);
    }
  }).then(function(customer){
    return Stripe.Charges.create({
      amount: request.params.amount, // in cents
      currency: request.params.currency,
      receipt_email: request.params.email,
      customer: customer.id
    },{
    success: function(results) {
      response.success(results);
    },
    error: function(httpResponse) {
      response.error(httpResponse);
    }
  });
  });
});

Parse.Cloud.define("changeOrder", function(request, response) {
  var Order = Parse.Object.extend("Order");
  var orderQuery =  new Parse.Query(Order);
  orderQuery.get(request.params.order, {
    success:function(order) {
      order.set("pay", true);
      order.set("payment", request.params.payment);
      order.save({
        success:function(result) {
          response.success(result);
        },
        error: function(error) {
          response.error(error);
        }
      });
    },
    error:function(error) {
      response.error(error);
    }
  });
});

function SendUserEmail (username,mentorname,useremail,confirm,pay,time,servicename,price,timezone,finish) {
  if (confirm == false && pay == false && finish == false) {

    var body = "Hello: " + username + "<br><br>";
    body += mentorname + " has been notified of your meeting request and will get back to you in 24-48 hours!<br><br>";
    body += "Time: " + time + " " + timezone + "<br>";
    body += "Service: " + servicename +"<br>";
    body += "Price: $ " + price +"<br><br>";
    body += "Please log into your dashboard on "+ "www.wallstreettequila.com" +" to check out the status<br><br>";
    body += "As always, we love to hear from you so please reach out to us at " + "info@wallstreettequila.com" + " anytime!"+"<br>Thank you!<br>";
    body += "This is non-reply email.";
    body += "<br>wallstreettequila<br><br>";
    body += "<p style='font-size:10px; color:#a1a1a1'>This e-mail message may contain confidential or legally privileged information and is intended only for the use of the intended recipient(s). Any unauthorized disclosure, dissemination, distribution, copying or the taking of any action in reliance on the information herein is prohibited. E-mails are not secure and cannot be guaranteed to be error free as they can be intercepted, amended, or contain viruses. Anyone who communicates with us by e-mail is deemed to have accepted these risks. WST is not responsible for errors or omissions in this message and denies any responsibility for any damage arising from the use of e-mail. Any opinion and other statement contained in this message and any attachment are solely those of the author and do not necessarily represent those of the company.</p>";
    var subject = "Meeting Verify";
  } else if (confirm == true && pay == false) {
    var body = "Hello: " + username + "<br><br>";
    body +="Great news! You and " + mentorname + " have found a time to talk. You have 24 hours to pay and secure this meeting before it expires.<br><br>";
    body += "Time: " + time + " " + timezone + "<br>";
    body += "Service: " + servicename +"<br>";
    body += "Price: $ " + price +"<br><br>";
    body += "Please log into your dashboard on " + "www.wallstreettequila.com" +" to make the payment<br><br>";
    body += "As always, we love to hear from you so please reach out to us at " + "info@wallstreettequila.com" + " anytime!"+"<br>Thank you!<br>";
    body += "This is non-reply email.";
    body += "<br>wallstreettequila<br><br>";
    body += "<p style='font-size:10px; color:#a1a1a1'>This e-mail message may contain confidential or legally privileged information and is intended only for the use of the intended recipient(s). Any unauthorized disclosure, dissemination, distribution, copying or the taking of any action in reliance on the information herein is prohibited. E-mails are not secure and cannot be guaranteed to be error free as they can be intercepted, amended, or contain viruses. Anyone who communicates with us by e-mail is deemed to have accepted these risks. WST is not responsible for errors or omissions in this message and denies any responsibility for any damage arising from the use of e-mail. Any opinion and other statement contained in this message and any attachment are solely those of the author and do not necessarily represent those of the company.</p>";
    var subject = "Meeting confirmed and payment required";
  } else if (confirm == true && pay == true && finish == false) {
    var body = "Hello: " + username + "<br><br>";
    body += "Congratulations! Your Payment is succeed. Please kindly find your reservation details blew:<br><br>";
    body += "Time: " + time + " " + timezone + "<br>";
    body += "Service: " + servicename +"<br>";
    body += "Price: $ " + price +"<br><br>";
    body += "Please add our dedicated WST Representative at WSTOffical on WeChat(or scan the QR code below). <br><br> She will connect you and your mentor within 24 hours of receiving your friend request.<br><br>";
    body += "<img src='https://www.wallstreettequila.com/upload/qrcode2.jpg' width='100' height='100' alt='QR Code' /><br><br>"
    body += "Please log into your dashboard on " + "www.wallstreettequila.com" +" to check out the status<br><br>";
    body += "Thank you so much for chooing WST and wish you an enriching and fruitful journey with WST! " 
    body += "If you have any inquiries or any suggestions, please feel free to email us at " + "info@wallstreettequila.com" + " , and we will reply as soon as possible.<br><br>";
    body += "For more information, please visit us at " + "www.wallstreettequila.com" + "<br><br>";
    body += "<p style='font-size:10px; color:#a1a1a1'>This e-mail message may contain confidential or legally privileged information and is intended only for the use of the intended recipient(s). Any unauthorized disclosure, dissemination, distribution, copying or the taking of any action in reliance on the information herein is prohibited. E-mails are not secure and cannot be guaranteed to be error free as they can be intercepted, amended, or contain viruses. Anyone who communicates with us by e-mail is deemed to have accepted these risks. WST is not responsible for errors or omissions in this message and denies any responsibility for any damage arising from the use of e-mail. Any opinion and other statement contained in this message and any attachment are solely those of the author and do not necessarily represent those of the company.</p>";
    var subject = "Payment Succeed";
  } else if (confirm == false && pay == false && finish == true){
    var body = "Hello: " + username + "<br><br>";
    body += "Your Order has benn canceled by mentor<br><br>";
    body += "Time: " + time + " " + timezone + "<br>";
    body += "Service: " + servicename +"<br>";
    body += "Price: $ " + price +"<br><br>";
    body += "Please log into your dashboard on " + "www.wallstreettequila.com" +" to check out the status and reason<br><br>";
    body += "As always, we love to hear from you so please reach out to us at " + "info@wallstreettequila.com" + " anytime!"+"<br>Thank you!<br>";
    body += "This is non-reply email.";
    body += "<br>wallstreettequila<br><br>";
    body += "<p style='font-size:10px; color:#a1a1a1'>This e-mail message may contain confidential or legally privileged information and is intended only for the use of the intended recipient(s). Any unauthorized disclosure, dissemination, distribution, copying or the taking of any action in reliance on the information herein is prohibited. E-mails are not secure and cannot be guaranteed to be error free as they can be intercepted, amended, or contain viruses. Anyone who communicates with us by e-mail is deemed to have accepted these risks. WST is not responsible for errors or omissions in this message and denies any responsibility for any damage arising from the use of e-mail. Any opinion and other statement contained in this message and any attachment are solely those of the author and do not necessarily represent those of the company.</p>";
    var subject = "Meeting canceled";
  } else {
    return false;
  }

  Mandrill.sendEmail({
                message: {
                  html: body,
                  subject: subject,
                  from_email: "no-reply@wallstreettequila.com",
                  from_name: "Wallstreettequila",
                  to: [
                    {
                      email: useremail,
                      name: username
                    }
                  ]
                },
                async: true
              },{
                success: function(httpResponse) {
                  console.log(httpResponse);
                  response.success("Your message is submited to us!");
                },
                error: function(httpResponse) {
                  console.error(httpResponse);
                  response.error("Uh oh, something went wrong");
                }
              });
}

function SendMentorEmail (username,mentorname,mentoremail,confirm,pay,time,servicename,price,finish,id) {
  if (confirm == false && pay == false && finish == false) {

    var body = "Hello: " + mentorname + "<br><br>";
    body += username + " has scheduled a meeting request, please verify this request in 24-48 hours!<br><br>";
    body += "Time: " + time +"<br>";
    body += "Service: " + servicename +"<br>";
    body += "Price: $ " + price +"<br><br>";
    body += "Please click following link to confirm this request <br><br>";
    body += "http://www.wallstreettequila.com/mentor.html?id=" + id + "<br><br>"
    body += "As always, we love to hear from you so please reach out to us at " + "info@wallstreettequila.com" + " anytime!"+"<br>Thank you!<br>";
    body += "This is non-reply email.";
    body += "<br>wallstreettequila<br><br>";
    body += "<p style='font-size:10px; color:#a1a1a1'>This e-mail message may contain confidential or legally privileged information and is intended only for the use of the intended recipient(s). Any unauthorized disclosure, dissemination, distribution, copying or the taking of any action in reliance on the information herein is prohibited. E-mails are not secure and cannot be guaranteed to be error free as they can be intercepted, amended, or contain viruses. Anyone who communicates with us by e-mail is deemed to have accepted these risks. WST is not responsible for errors or omissions in this message and denies any responsibility for any damage arising from the use of e-mail. Any opinion and other statement contained in this message and any attachment are solely those of the author and do not necessarily represent those of the company.</p>";
    var subject = "Meeting Verify";
  } else if (confirm == true && pay == false && finish == false) {
    var body = "Hello: " + mentorname + "<br><br>";
    body +="Great news! You and " + username + " have found a time to talk. You just need to wait for the confirm from " + username +"<br><br>";
    body += "Time: " + time +"<br>";
    body += "Service: " + servicename +"<br>";
    body += "Price: $ " + price +"<br><br>";
    body += "Please click following link to check status <br><br>";
    body += "http://www.wallstreettequila.com/mentor.html?id=" + id + "<br><br>"
    body += "As always, we love to hear from you so please reach out to us at " + "info@wallstreettequila.com" + " anytime!"+"<br>Thank you!<br>";
    body += "This is non-reply email.";
    body += "<br>wallstreettequila<br><br>";
    body += "<p style='font-size:10px; color:#a1a1a1'>This e-mail message may contain confidential or legally privileged information and is intended only for the use of the intended recipient(s). Any unauthorized disclosure, dissemination, distribution, copying or the taking of any action in reliance on the information herein is prohibited. E-mails are not secure and cannot be guaranteed to be error free as they can be intercepted, amended, or contain viruses. Anyone who communicates with us by e-mail is deemed to have accepted these risks. WST is not responsible for errors or omissions in this message and denies any responsibility for any damage arising from the use of e-mail. Any opinion and other statement contained in this message and any attachment are solely those of the author and do not necessarily represent those of the company.</p>";
    var subject = "Meeting confirmed Waiting";
  } else if (confirm == true && pay == true && finish == false) {
    var body = "Hello: " + mentorname + "<br><br>";
    body += username +"'s meeting request is succeed.<br><br>";
    body += "Time: " + time +"<br>";
    body += "Service: " + servicename +"<br>";
    body += "Price: $ " + price +"<br><br>";
    body += "Please click following link to download and upload files <br><br>";
    body += "http://www.wallstreettequila.com/mentor.html?id=" + id + "<br><br>"
    body += "As always, we love to hear from you so please reach out to us at " + "info@wallstreettequila.com" + " anytime!"+"<br>Thank you!<br>";
    body += "This is non-reply email.";
    body += "<br>wallstreettequila<br><br>";
     body += "<p style='font-size:10px; color:#a1a1a1'>This e-mail message may contain confidential or legally privileged information and is intended only for the use of the intended recipient(s). Any unauthorized disclosure, dissemination, distribution, copying or the taking of any action in reliance on the information herein is prohibited. E-mails are not secure and cannot be guaranteed to be error free as they can be intercepted, amended, or contain viruses. Anyone who communicates with us by e-mail is deemed to have accepted these risks. WST is not responsible for errors or omissions in this message and denies any responsibility for any damage arising from the use of e-mail. Any opinion and other statement contained in this message and any attachment are solely those of the author and do not necessarily represent those of the company.</p>";
    var subject = "Meeting Succeed";
  } else {
    return false;
  }

  Mandrill.sendEmail({
                message: {
                  html: body,
                  subject: subject,
                  from_email: "no-reply@wallstreettequila.com",
                  from_name: "Wallstreettequila",
                  to: [
                    {
                      email: mentoremail,
                      name: mentorname
                    }
                  ]
                },
                async: true
              },{
                success: function(httpResponse) {
                  console.log(httpResponse);
                  response.success("Your message is submited to us!");
                },
                error: function(httpResponse) {
                  console.error(httpResponse);
                  response.error("Uh oh, something went wrong");
                }
              });
}


Parse.Cloud.define("SignUp", function(request, response) {
              var user = new Parse.User();
              user.set("username", request.params.username);
              user.set("password", request.params.password);
              user.set("email", request.params.email);
              user.set("wechat", request.params.wechat);
              user.set("university", request.params.university);
              user.set("major", request.params.major);
              user.set("name", request.params.name);
              user.set("country", request.params.country);
              user.set("phone", request.params.phone);
              user.set("mentor", false);
              if(request.params.file !='') {
                user.set("img", request.params.file);
              }
              user.signUp(null, {
                  success: function(user) {
                      //loggedInUser = user;
                      response.success(user);
                  },
                  error: function(user, error) {
                    response.error("Error: " + error.message);
                  }
              });
});

Parse.Cloud.define("UserInfo", function(request, response) {
  var query = new Parse.Query(Parse.User);
  query.get(request.params.user, {
    success:function(user){
      response.success(user);
    },
    error:function(error){
      response.error(error);
    }
  });
});

Parse.Cloud.define("Update", function(request, response) {
  var query = new Parse.Query(Parse.User);
  query.get(request.params.user, {
    success: function(user) {
                user.set("username", request.params.username);
                user.set("university", request.params.university);
                user.set("major", request.params.major);
                user.set("wechat", request.params.wechat);
                user.set("email", request.params.email);
                user.set("name", request.params.name);
                user.set("country", request.params.country);
                user.set("phone", request.params.phone);
                user.set("img", request.params.file);
                if (request.params.password == "") {
                  user.save()
                  .then(
                    function(user) {
                      //console.log(user);
                      response.success("nopass");
                    },
                    function(error) {
                      response.error("Error: " + error.message);
                    });
                } else {
                  Parse.Cloud.useMasterKey();
                  user.set("password",request.params.password);
                  user.save()
                  .then(
                    function(user) {
                      response.success("pass");
                    },
                    function(error) {
                      response.error("Error: " + error.message);
                    });
                }
    },
    error: function(error) {
      response.error("Error: " + error.message);
    }
  });
});


Parse.Cloud.afterSave("Order", function(request) {
  var name = request.object.get('menteeName');
  var userEmail = request.object.get('menteeEmail');
  var mentor = request.object.get('mentorName');
  var mentorEmail = request.object.get('mentorEmail');
  var confirm = request.object.get('confirm');
  var pay = request.object.get('pay');
  var objectTime = request.object.get('Time');
  var servicename = request.object.get('serviceName');
  var price = request.object.get('price');
  var timezone = request.object.get("timezone");
  var jun = moment(objectTime);
  var finish = request.object.get('finish');
  var id = request.object.get("Mentor").id;
  console.log(id);
  if (timezone == "EDT") {
    
    var newTime = jun.clone().tz("America/New_York");
    var time = newTime.format('llll');
  }
  if (timezone == "CDT") {
    var newTime = jun.clone().tz("America/Chicago");
    var time = newTime.format('llll');
  }
  if (timezone == "MDT") {
    var newTime = jun.clone().tz("America/Denver");
    var time = newTime.format('llll');
  }
  if (timezone == "PDT") {
    var newTime = jun.clone().tz("America/Los_Angeles");
    var time = newTime.format('llll');
  }
  if (timezone == "HKT") {
    var newTime = jun.clone().tz("Asia/Hong_Kong");
    var time = newTime.format('llll');
  }
  if (timezone == "GMT") {
    var newTime = jun.clone().tz("Europe/London");
    var time = newTime.format('llll');
  }
  if (timezone == "CST") {
    var newTime = jun.clone().tz("Asia/Shanghai");
    var time = newTime.format('llll');
  }
  SendUserEmail(name,mentor,userEmail,confirm,pay,time,servicename,price,timezone,finish);
  SendMentorEmail (name,mentor,mentorEmail,confirm,pay,time,servicename,price,finish,id);
});

Parse.Cloud.define("getReviews", function(request, response) {
  var Review = Parse.Object.extend("Review");
  var reviewQuery = new Parse.Query(Review);
  reviewQuery.equalTo("product", request.params.id);
  reviewQuery.find({
    success:function(results) {
      response.success(results);
    },
    error:function(error) {
      response.error(error);
    }
  })
});

Parse.Cloud.define("sendEmail", function(request, response) {
  var name = request.params.name;
  var email = request.params.email;
  var school = request.params.school;
  var subject = request.params.subject;
  var message = request.params.message;

  var body = "name: " + name + "<br>";
    body +="email: " + email + "<br>";
    body += "school: " + school + "<br>";
    body += "subject: " + subject + "<br>";
    body += "message: " + message +"<br>";

  Mandrill.sendEmail({
                message: {
                  html: body,
                  subject: name + "" + subject,
                  from_email: "no-reply@wallstreettequila.com",
                  from_name: "Wallstreettequila",
                  to: [
                    {
                      email: "info@wallstreettequila.com",
                      name: "Wallstreettequila"
                    }
                  ]
                },
                async: true
              },{
                success: function(httpResponse) {
                  console.log(httpResponse);
                  response.success("Your message is submited to us!");
                },
                error: function(httpResponse) {
                  console.error(httpResponse);
                  response.error("Uh oh, something went wrong");
                }
              });
});

Parse.Cloud.job("checkExpire", function(request, status) {
  // Set up to modify user data
  var Order = Parse.Object.extend("Order");
  var query = new Parse.Query(Order);
  query.each(function(order) {
      // Update to plan value passed in
      var expire = order.get('ExpireTime');
      var confirm = order.get('confirm');
      if(!confirm){
        if(moment().isAfter(expire)) {
          order.set("finish",true);
          return order.save();
        }
      }   
      
  }).then(function() {
    // Set the job's success status
    status.success("Migration completed successfully.");
  }, function(error) {
    // Set the job's error status
    status.error("Uh oh, something went wrong.");
  });
});