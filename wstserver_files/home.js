
jQuery("#loginForm").validationEngine('attach', {promptPosition : "centerRight", autoPositionUpdate : true,showOneMessage: true});
jQuery("#menteeRegisterForm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true,showOneMessage: true});
jQuery("#mentorForm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true,showOneMessage: true});
jQuery("#contactUsForm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true,showOneMessage: true});
jQuery("#forgotPwdForm").validationEngine('attach', {promptPosition : "bottomLeft", autoPositionUpdate : true,showOneMessage: true});

//年范围
var _curdate = new Date();
var _year = _curdate.getFullYear();

$(function(i){
	for(var i = parseInt(_year); i >= 1990; i--){
		$(".select-year").append("<li data-value='"+i+"'>"+i+"</li>");
	}
	isLogin();
})

function initYearSelect(){
	$(".select-year").html('');
	for(var i = parseInt(_year); i >= 1990; i--){
		$(".select-year").append("<li data-value='"+i+"'>"+i+"</li>");
	}
}

var portal_name = null;
$(function(){
	$.ajax({
		   type: "POST",
		   url: path+"/offerRecord/queryByPage.action",
		   dataType:'json',
		   data : {'page':'1','rows':'20'},
		   success: function(msg){
			   if(msg && msg.state == 'SUCCESS'){
				 
				   if(msg.content && msg.content.rows){
					   var offerArr = msg.content.rows;
					   for(var i = 0; i < offerArr.length; i++){
						   var contentDiv = ' <div class="home-offersub-item">'
								 +'<div class="home-offersub-item-mask">'
								 +'	<div class="home-offersub-item-mask-title">Mentor</div>'
								 +'	<div class="home-offersub-item-mask-mentor">'+offerArr[i].mentorName+'</div>'
								 +'</div>'
								 +'<div class="home-offersub-item-inner">'
								 +'	<div class="home-offersub-item-name">'+offerArr[i].menteeName+'</div>'
								 +'	<div class="home-offersub-item-desc">'+offerArr[i].remark+'</div>'
								 +'</div>'
							     +'</div>';
						   $("#offerListDiv").prepend(contentDiv);
					   }
				   }
			   }
		   }
		});
})

function fogotFormSubmit(){
	var result = $("#forgotPwdForm").validationEngine("validate");
	if(result){
		$('#forgotPwdForm').ajaxSubmit({
			dataType: 'json',
			type : 'post',
			url:path+'/portal/sendEmailToUpdate.action',
			success :function(result) {
				//操作成功
				if(dataResult(result)){
					$("#fogot-tip").html("A confirmation email has been sent.");
					window.setTimeout(function(){
						$(".j-alert-close").click();
					}, "1000")
			   }else{
				   $("#fogot-tip").html("<font color='#FF3300'>"+result.message+"</font>");
			   }
			}
		});
	}
}

//mentee注册
function menteeSubmit(){
	var result = $("#menteeRegisterForm").validationEngine("validate");
	if(result){
		loading('mentee-loading');
		$('#menteeRegisterForm').ajaxSubmit({
			dataType: 'json',
			type : 'post',
			success :function(result) {
				stop('mentee-loading');
				//操作成功
				if(dataResult(result)){
					$("#mentee-loading").html("A confirmation email has been sent.");
			   }else{
				   $("#mentee-loading").html("<font color='#FF3300'>"+result.message+"</font>");
			   }
			}
		});
	}
}

function validateMentorStep1(){
	var r1 = $("#mfirstName").validationEngine("validate");
	var r2 = $("#mlastName").validationEngine("validate");
	var r3 = $("#memail").validationEngine("validate");
	var r4 = $("#mwechat").validationEngine("validate");
	var r5 = $("#mphone").validationEngine("validate");
	var r6 = $("#mpassword").validationEngine("validate");
	var r7 = $("#mconfirmPassword").validationEngine("validate");
	
	if(!r1 && !r2 && !r3 && !r4 && !r5 && !r6 && !r7){
		return true;
	}else{
		return false;
	}
}

function mentorSubmit(){
	var result = $("#mentorForm").validationEngine("validate");
	if(result){
		loading('mentor-loading');
		$('#mentorForm').ajaxSubmit({
			dataType: 'json',
			type : 'post',
			success :function(result) {
				stop('mentor-loading');
				//操作成功
				if(dataResult(result)){
					$("#mentor-loading").html("A confirmation email has been sent.");
			   }else{
				   $("#mentor-loading").html("<font color='#FF3300'>"+result.message+"</font>");
			   }
			}
		});
	}
}


//联系我们form提交
function contactUsSubmit(){
	var result = $("#contactUsForm").validationEngine("validate");
	if(result){
		loading('contact-loading');
		$('#contactUsForm').ajaxSubmit({
			dataType: 'json',
			type : 'post',
			success :function(result) {
				stop('contact-loading');
				//操作成功
				if(dataResult(result)){
					$("#contact-loading").html("A contaction email has been sent.");
			   }else{
				   $("#contact-loading").html("<font color='#FF3300'>"+result.message+"</font>");
			   }
			}
		});
	}
}

//登陆提交
function login(){
	var result = $("#loginForm").validationEngine("validate");
	if(result){
		loading('login-loading');
		$('#loginForm').ajaxSubmit({
			dataType: 'json',
			type : 'post',
			success :function(result) {
				stop('login-loading');
				//登陆成功
				if(dataResult(result)){
				  if(result.content){
					  var url = "";
					  if(result.content.userType == 'mentor'){
						  url=path+"/mentor/permission/detail.action?mentorId="+result.content.userId;
					  }else if(result.content.userType == 'mentee'){
						  url=path+"/link.action?page=mentee";
					  }else{
						  $("#login-loading").html("The account is't exist.");
						  return;
					  }
					  //var toploginnav = "<a href='javascript:;' class='topnav-nav-list-login j-login-btn'>Login</a><a href='javascript:;' class='j-signup-btn active'>Sign Up</a>";
					  $("#top-loginnav").html('');
					  $("#home-loginnav").html('');
					  
					  var toplogoutnav = "<a href='"+url+"' class='topnav-nav-list-login j-login-btn active'>My Account</a><a href='"+path+"/portal/logout.action' class='j-signup-btn'>Logout</a>";
					  $("#top-loginnav").append(toplogoutnav);
					  $("#home-loginnav").append(toplogoutnav);
					  $("#login-loading").html("<font >Login success.<font>");
					  
				    var $chooseDom = $('.j-choose');
					var $displayDom = $('.j-display');

					var $loginFormDom = $('.j-login-form');
					var $menteeRegFormDom = $('.j-mentee-form');
					var $mentorRegFormDom = $('.j-mentor-form');
					var $emailDom = $displayDom.find('.home-account-right-choose-email');
					  
					$chooseDom.fadeOut(500);
					$menteeRegFormDom.fadeOut(500);
					$mentorRegFormDom.fadeOut(500);
					$loginFormDom.fadeOut(500);
					window.setTimeout(function() {
						$displayDom.fadeIn(500);
						$emailDom.text('Welcome,'+result.content.userName);
						$emailDom.show();
					}, 600);
				  }
			   }else{
				  $("#login-loading").html("<font color='#FF3300'>"+result.message+"</font>");
			   }
			}
		});
	}
}

function isLogin(){
	$.post(path + "/portal/isLogin.action", {}, function(result){
		result = eval("result=" + result);
		// 判断是否已登陆
		if(dataResult(result)){
		  if(result.content){
			  var url = "";
			  if(result.content.userType == 'mentor'){
				  url=path+"/mentor/permission/detail.action?mentorId="+result.content.userId;
			  }else if(result.content.userType == 'mentee'){
				  url=path+"/link.action?page=mentee";
			  }else{
				  $("#login-loading").html("The account is't exist.");
				  return;
			  }
			  //var toploginnav = "<a href='javascript:;' class='topnav-nav-list-login j-login-btn'>Login</a><a href='javascript:;' class='j-signup-btn active'>Sign Up</a>";
			  $("#top-loginnav").html('');
			  $("#home-loginnav").html('');
			  
			  var toplogoutnav = "<a href='"+url+"' class='topnav-nav-list-login j-login-btn active'>My Account</a><a href='"+path+"/portal/logout.action' class='j-signup-btn'>Logout</a>";
			  $("#top-loginnav").append(toplogoutnav);
			  $("#home-loginnav").append(toplogoutnav);
			  $("#login-loading").html("<font >Login success.<font>");
			  
		    var $chooseDom = $('.j-choose');
			var $displayDom = $('.j-display');

			var $loginFormDom = $('.j-login-form');
			var $menteeRegFormDom = $('.j-mentee-form');
			var $mentorRegFormDom = $('.j-mentor-form');
			var $emailDom = $displayDom.find('.home-account-right-choose-email');
			  
			$chooseDom.fadeOut(500);
			$menteeRegFormDom.fadeOut(500);
			$mentorRegFormDom.fadeOut(500);
			$loginFormDom.fadeOut(500);
			window.setTimeout(function() {
				$displayDom.fadeIn(500);
				$emailDom.text('Welcome,'+result.content.userName);
				$emailDom.show();
			}, 600);
		  }
	   }else{
		  $("#login-loading").html("<font color='#FF3300'>"+result.message+"</font>");
	   }
	});
}

//申请
function applyRecord(self){
	var $apply = $(self);
	var programer = $apply.attr('data');
	
	if(programer){
		$.ajax({
		   type: "POST",
		   url: path+"/applyRecord/add.action",
		   dataType:'json',
		   data : {'programer':programer},
		   success: function(msg){
			   $apply.text(msg.message);
			   if(msg && msg.state == 'SUCCESS'){
				   $apply.live('click', function(event) {
					   event.preventDefault();   
				   });
				   $apply.unbind('click');
			   }
		   }
		});
	}
}


//设置文件名称
function setUploadText(resumeFile,resumeText){
	var file = $("#"+resumeFile).val();
	$("#"+resumeText).text(file);
	
}

var loadingInterval;
function loading(tip){
	var total = 0;
	loadingInterval = window.setInterval(function(){
		if(total < 10){
			$("#"+tip).append(".");
		}else{
			$("#"+tip).text("");
			total=0;
		}
		total++;
	}, 1000);	
}

function stop(tip){
	$("#"+tip).text("");
	clearInterval(loadingInterval);	
}

